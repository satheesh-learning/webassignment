const express = require('express');
const bodyparse = require('body-parser');
const mysql = require('mysql');

const app = express();
app.use(bodyparse.urlencoded({ extended: false }));
app.use(bodyparse.json());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Request-Method', '*');
    res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST,DELETE');
    res.header('Access-Control-Allow-Headers', '*');
    next();
});

const connection = mysql.createConnection({

  host: '192.168.99.100',
  user: 'appuser',
  password: 'Abcdefgh123',
  database: 'rocksdb'

});
connection.connect((err) => {
    if (err) throw err;
    console.log('Connected!');
});

app.delete('/:uname', function (req, res) {
    connection.query("delete from users_data where userName=(?)", [req.params.uname], function (error, result) {
        if (error) {
            res.status(400).send(error)
        }
        else {
            res.send("rec deleted")
        }
    });
})

app.post('/', function (req, res) {
    var newarray = [req.body.name, req.body.username, req.body.password, req.body.gender, req.body.address]
    connection.query("insert into users_data (name,userName, password, gender, address) values (?)", [newarray], function (error, results) {

      if (error) { res.status(400).send(error); }
      else {
        console.log('The result is: ', results);
        let result = [{ 'name': req.body.name, 'username': req.body.username, 'password': req.body.password, 'gender': req.body.gender, 'address': req.body.address }]
        res.send(result)
      
    });
})

app.put('/:uname', function (req, res) {
    connection.query("update users_data set password=?, address=? where userName=?", [req.body.password, req.body.address, req.params.uname], function (error, results) {
        if (error) { res.status(400).send(error); }
        else {
            console.log('The result is: ', results);
            let result = [{ 'name': req.body.name, 'username': req.body.userId, 'password': req.body.password, 'gender': req.body.gender, 'address': req.body.address }]
            res.send(result)
        }
    });
})

app.get('/', function (req, res) {
    connection.query("select * from users_data ", function (error1, results1) {
        if (error1) res.status(400).send(error1);
        res.send(results1)
    });
});

app.post('/login', function (req, res) {
    var username = [req.body.name];
    console.log(username);
    var password = [req.body.password];
    console.log(password);
    connection.query("select * from users_data where userName = ? and password = ?", [username, password], function (error, results1) {
        if (error) {
            res.status(501).send(error);
        }
        else {
            if (results1.length === 0) {
                res.status(401);
            }
            res.send(results1);
            console.log("resultText" + results1);
        }
    });
})

app.listen(3010)